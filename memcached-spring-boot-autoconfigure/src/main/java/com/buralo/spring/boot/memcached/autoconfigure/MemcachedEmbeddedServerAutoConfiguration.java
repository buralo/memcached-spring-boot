/*
 *  Copyright 2018 Búraló Technologies.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package com.buralo.spring.boot.memcached.autoconfigure;

import com.thimbleware.jmemcached.CacheImpl;
import com.thimbleware.jmemcached.Key;
import com.thimbleware.jmemcached.LocalCacheElement;
import com.thimbleware.jmemcached.MemCacheDaemon;
import com.thimbleware.jmemcached.storage.CacheStorage;
import com.thimbleware.jmemcached.storage.hash.ConcurrentLinkedHashMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConditionalOnClass(MemCacheDaemon.class)
@EnableConfigurationProperties(MemcachedProperties.class)
public class MemcachedEmbeddedServerAutoConfiguration {

    @Bean(initMethod = "start", destroyMethod = "stop")
    @Autowired
    @ConditionalOnMissingBean
    public MemCacheDaemon<LocalCacheElement> embeddedMemcachedDaemon(final MemcachedProperties properties) {
        final MemcachedProperties.Embedded embedded = properties.getEmbedded();
        final CacheStorage<Key, LocalCacheElement> storage = ConcurrentLinkedHashMap.create(
                ConcurrentLinkedHashMap.EvictionPolicy.FIFO,
                embedded.getMaxItems(),
                embedded.getMaxBytes(),
                embedded.getThreads());
        final MemCacheDaemon<LocalCacheElement> daemon = new MemCacheDaemon<>();
        daemon.setCache(new CacheImpl(storage));
        daemon.setBinary(embedded.isBinary());
        daemon.setIdleTime(embedded.getIdleTime());
        daemon.setAddr(Utils.toAddress(embedded.getAddress()));
        daemon.setVerbose(embedded.isVerbose());
        return daemon;
    }
}
