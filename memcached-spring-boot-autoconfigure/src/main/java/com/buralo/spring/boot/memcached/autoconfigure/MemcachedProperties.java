/*
 *  Copyright 2018 Búraló Technologies.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package com.buralo.spring.boot.memcached.autoconfigure;

import org.springframework.boot.context.properties.ConfigurationProperties;

import java.util.Collections;
import java.util.List;

@ConfigurationProperties("memcached")
public class MemcachedProperties {

    private List<Server> servers = Collections.singletonList(new Server());

    private Embedded embedded = new Embedded();

    public List<Server> getServers() {
        return servers;
    }

    public void setServers(final List<Server> servers) {
        this.servers = servers;
    }

    public Embedded getEmbedded() {
        return embedded;
    }

    public void setEmbedded(final Embedded embedded) {
        this.embedded = embedded;
    }

    public static class Embedded {
        private int maxItems = 1024;

        private long maxBytes = 1048576;

        private int threads = 16;

        private Server address = new Server();

        private boolean verbose;

        private boolean binary;

        private int idleTime;

        public int getMaxItems() {
            return maxItems;
        }

        public void setMaxItems(final int maxItems) {
            this.maxItems = maxItems;
        }

        public long getMaxBytes() {
            return maxBytes;
        }

        public void setMaxBytes(final long maxBytes) {
            this.maxBytes = maxBytes;
        }

        public int getThreads() {
            return threads;
        }

        public void setThreads(final int threads) {
            this.threads = threads;
        }

        public Server getAddress() {
            return address;
        }

        public void setAddress(final Server address) {
            this.address = address;
        }

        public boolean isVerbose() {
            return verbose;
        }

        public void setVerbose(final boolean verbose) {
            this.verbose = verbose;
        }

        public boolean isBinary() {
            return binary;
        }

        public void setBinary(final boolean binary) {
            this.binary = binary;
        }

        public int getIdleTime() {
            return idleTime;
        }

        public void setIdleTime(final int idleTime) {
            this.idleTime = idleTime;
        }

    }

    public static class Server {

        private String hostname;

        private int port = -1;

        public String getHostname() {
            return hostname;
        }

        public void setHostname(final String hostname) {
            this.hostname = hostname;
        }

        public int getPort() {
            return port;
        }

        public void setPort(final int port) {
            this.port = port;
        }
    }
}
