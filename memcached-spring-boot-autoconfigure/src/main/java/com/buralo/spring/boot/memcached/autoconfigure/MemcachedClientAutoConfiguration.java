/*
 *  Copyright 2018 Búraló Technologies.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package com.buralo.spring.boot.memcached.autoconfigure;

import net.spy.memcached.ConnectionFactory;
import net.spy.memcached.MemcachedClient;
import net.spy.memcached.MemcachedClientIF;
import org.springframework.beans.factory.ObjectProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.util.List;
import java.util.stream.Collectors;

@Configuration
@ConditionalOnClass(MemcachedClientIF.class)
@EnableConfigurationProperties(MemcachedProperties.class)
public class MemcachedClientAutoConfiguration {

    @Bean
    @Autowired
    @Lazy
    @ConditionalOnMissingBean
    public MemcachedClientIF memcachedClient(final MemcachedProperties properties,
                                             final ObjectProvider<ConnectionFactory> connectionFactories) throws IOException {
        final List<InetSocketAddress> addresses = properties.getServers()
                .stream()
                .map(Utils::toAddress)
                .collect(Collectors.toList());
        final ConnectionFactory connectionFactory = connectionFactories.getIfUnique();
        return connectionFactory == null
                ? new MemcachedClient(addresses)
                : new MemcachedClient(connectionFactory, addresses);
    }
}
