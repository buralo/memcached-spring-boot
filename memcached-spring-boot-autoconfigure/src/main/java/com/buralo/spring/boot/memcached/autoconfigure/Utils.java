/*
 *  Copyright 2018 Búraló Technologies.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package com.buralo.spring.boot.memcached.autoconfigure;

import java.net.InetSocketAddress;

class Utils {

    private static final String DEFAULT_HOST = "localhost";

    private static final int DEFAULT_PORT = 11211;

    private Utils() {
    }

    static InetSocketAddress toAddress(final MemcachedProperties.Server server) {
        return toAddress(server.getHostname(), server.getPort());
    }

    private static InetSocketAddress toAddress(final String host,
                                       final int port) {
        return new InetSocketAddress(
                host == null ? DEFAULT_HOST: host,
                port == -1 ? DEFAULT_PORT : port);
    }
}
