# Spring Boot Starter for Memcached

## Overview

The **Memcached Spring Boot Starter** is a [Spring Boot](https://spring.io/projects/spring-boot) starter that provides
the dependencies for and auto-configures a [Memcached](https://memcached.org/) client. 

The **Memcached Test Spring Boot Starter** is a [Spring Boot](https://spring.io/projects/spring-boot) starter that 
provides the dependencies for and auto-configures an embedded [Memcached](https://memcached.org/) server using
[jmemcached-daemon](https://github.com/rdaum/jmemcache-daemon).  

## Configuring the Memcached client

The table below describes the properties that can be can be used to configure the Memcached client.

Property                         | Default Value | Description
-------------------------------- | ------------- | -----------
memcached.servers[n].host        | localhost     | The host name of the memcached servers
memcached.servers[n].port        | 11211         | The port number of the memcached server

## Configuring an embedded Memcached server

The table below describes the properties that can be used to configure an embedded Memcached server.

Property                         | Default Value | Description
-------------------------------- | ------------- | -----------
memcached.embedded.address.host  | localhost     | The host name of the embedded memcached server
memcached.embedded.address.post  | 11211         | The port number of the embedded memcached server
memcached.embedded.max-items     | 1024          | The maximum number of items in the cache
memcached.embedded.max-bytes     | 1048576       | The maximum size of the cache (in bytes) 
memcached.embedded.threads       | 16            | The number of concurrent threads
memcached.embedded.verbose       | false         | 
memcached.embedded.binary        | false         |
memcached.embedded.idle-time     | 0             |

## Maven Central Coordinates

The **Memcached Spring Boot Starter** has been published in [Maven Central](http://search.maven.org) at the following 
coordinates:

```xml
<dependency>
    <groupId>com.buralo.spring.boot.memcached</groupId>
    <artifactId>memcached-spring-boot-starter</artifactId>
    <version>1.1.0</version>
</dependency>
```

The **Memcached Test Spring Boot Starter** has been published in [Maven Central](http://search.maven.org) at the 
following coordinates:

```xml
<dependency>
    <groupId>com.buralo.spring.boot.memcached</groupId>
    <artifactId>memcached-test-spring-boot-starter</artifactId>
    <version>1.1.0</version>
</dependency>
```

License & Source Code
---------------------
The **Memcached Spring Boot Starter** is made available under the
[Apache License](http://www.apache.org/licenses/LICENSE-2.0.html) and the source code is hosted on
[GitHub](http://github.com) at https://gitlab.com/buralo/memcached-spring-boot.

